# Sciter FFmpeg
A Sciter FFmpeg is a sciter behavior component. It can be used to playback local/remote media files in your sciter app's UI.

The goal is to improve \<video\>、\<audio\> controller (as Sciter's behavior) so Sciter will be able to playback media files for almost all platforms.

## Features
* Windows / macOS / Linux - using alternative [media-playback](https://github.com/obsproject/obs-studio/tree/master/deps/media-playback) and FFmpeg files (compiled by [avbuild](https://github.com/wang-bin/avbuild) or your own)
* Supports playback local/remote media files
* Supports methods: load, play, pause, stop
* Supports attributes: duration, currentTime, playbackRate, volume

## Platforms
* Windows - i32, i64
* macOS - i64, arm64
* Linux - i64

## Usage
External Behavior: 
1) Build dll/dylib/so file with projects in folder 'build';
2) Put dll/dylib/so file into sciter application running folder;
3) Put FFmpeg files into sciter application running folder;
4) Use \<video\> and define in css: video {behavior: ffmpeg library(sciter-ffmpeg) video;}
   Use \<audio\> and define in css: audio {behavior: ffmpeg library(sciter-ffmpeg);}

Internal Behavior: 
1) Copy all files in folder [source](https://gitlab.com/ArcRain/sciter-ffmpeg/-/tree/main/source) into sciter-js-sdk [include/behaviors](https://gitlab.com/sciter-engine/sciter-js-sdk/-/tree/main/include/behaviors)
2) Compile application project with 'vendors' libraries;
3) Put FFmpeg files into sciter application running folder;
4) Use \<video\> and define in css: video {behavior: ffmpeg video;}
   Use \<audio\> and define in css: audio {behavior: ffmpeg;}

## Reference & Acknowledgment
[Sciter](https://gitlab.com/sciter-engine/sciter-js-sdk) - Build cross platform desktop applications with HTML, CSS and javascript.

[FFmpeg](https://ffmpeg.org) - A complete, cross-platform solution to record, convert and stream audio and video.

[media-playback](https://github.com/obsproject/obs-studio/tree/master/deps/media-playback) - It's a dependency core used by OBS Studio

[avbuild](https://github.com/wang-bin/avbuild) - It's a tool to build ffmpeg for almost all platforms.