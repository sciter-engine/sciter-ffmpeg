/*
 * Copyright (c) 2017 Hugh Bailey <obs.jim@gmail.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#pragma once

#include <thread>
#include "sciter-x-def.h"
#include "sciter-x-threads.h"

#include "decode.h"

#include <darray.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244)
#pragma warning(disable : 4204)
#endif

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#define MAX_AV_PLANES 8

	struct obs_source_frame {
		uint8_t* data[MAX_AV_PLANES];
		uint32_t linesize[MAX_AV_PLANES];
		uint32_t width;
		uint32_t height;
		uint64_t timestamp;

		AVPixelFormat format;
		float color_matrix[16];
		bool full_range;
		uint16_t max_luminance;
		float color_range_min[3];
		float color_range_max[3];
		bool flip;
		uint8_t flags;

		/* used internally by libobs */
		volatile long refs;
		bool prev_frame;
	};

	struct obs_source_audio {
		const uint8_t* data[MAX_AV_PLANES];
		uint32_t frames;

		int channels;
		AVSampleFormat format;
		uint32_t samples_per_sec;

		uint64_t timestamp;
	};

	typedef void (*mp_video_cb)(void* opaque, struct obs_source_frame* frame);
	typedef void (*mp_audio_cb)(void* opaque, struct obs_source_audio* audio);
	typedef void (*mp_metadata_cb)(void* opaque);
	typedef void (*mp_timeupdate_cb)(void* opaque);
	typedef void (*mp_stop_cb)(void* opaque);
	typedef void (*mp_ready_cb)(void* opaque);
	typedef void (*mp_error_cb)(void* opaque, int code);
	typedef void (*mp_seek_cb)(void* opaque, bool seeked);

	struct mp_media {
		AVFormatContext* fmt;

		mp_video_cb v_preload_cb;
		mp_video_cb v_seek_cb;
		mp_metadata_cb metadata_cb;
		mp_stop_cb stop_cb;
		mp_video_cb v_cb;
		mp_audio_cb a_cb;
		mp_seek_cb seek_cb;
		mp_ready_cb ready_cb;
		mp_timeupdate_cb timeupdate_cb;
		mp_error_cb error_cb;
		void* opaque;

		char* path;
		char* format_name;
		char *ffmpeg_options;
		int buffering;
		int speed;

		enum AVPixelFormat scale_format;
		struct SwsContext* swscale;
		int scale_linesizes[4];
		uint8_t* scale_pic[4];

		DARRAY(AVPacket *) packet_pool;
		struct mp_decode v;
		struct mp_decode a;
		bool is_local_file;
		bool has_seek;
		bool reconnecting;
		bool has_video;
		bool has_audio;
		bool eof;
		bool hw;

		struct obs_source_frame obsframe;
		AVColorSpace cur_space;
		AVColorRange cur_range;
		AVColorRange force_range;
		bool is_linear_alpha;

		int64_t play_sys_ts;
		int64_t next_pts_ns;
		uint64_t next_ns;
		int64_t start_ts;
		int64_t base_ts;

		uint64_t interrupt_poll_ts;

		sciter::sync::mutex* mutex;
		sciter::sync::event* sem;

		bool stopping;
		bool looping;
		bool active;
		bool reset;
		bool kill;

		bool thread_valid;
		std::thread* thread;

		bool pause;
		bool reset_ts;
		bool seek;
		bool seek_next_ts;
		int64_t seek_pos;
	};

	typedef struct mp_media mp_media_t;

	struct mp_media_info {
		void* opaque;

		mp_video_cb v_cb;
		mp_video_cb v_preload_cb;
		mp_video_cb v_seek_cb;
		mp_metadata_cb metadata_cb;
		mp_audio_cb a_cb;
		mp_stop_cb stop_cb;
		mp_ready_cb ready_cb;
		mp_timeupdate_cb timeupdate_cb;
		mp_error_cb error_cb;
		mp_seek_cb seek_cb;

		const char* path;
		const char* format;
		const char *ffmpeg_options;
		int buffering;
		int speed;
		AVColorRange force_range;
		bool is_linear_alpha;
		bool hardware_decoding;
		bool is_local_file;
		bool reconnecting;
	};

	extern bool mp_media_init(mp_media_t* media, const struct mp_media_info* info);
	extern void mp_media_free(mp_media_t* media);

	extern void mp_media_play(mp_media_t* media, bool loop, bool reconnecting);
	extern void mp_media_stop(mp_media_t* media);
	extern void mp_media_play_pause(mp_media_t* media, bool pause);
	extern int64_t mp_media_get_current_time(mp_media_t* m);
	extern int64_t mp_media_get_frames(mp_media_t* m, AVMediaType mediaType);
	extern int64_t mp_media_get_duration(mp_media_t* m);
	extern void mp_media_seek(mp_media_t* m, int64_t pos);
	extern void mp_media_set_speed(mp_media_t* m, int speed);
	extern int mp_media_get_speed(mp_media_t* m);

/* #define DETAILED_DEBUG_INFO */

#ifdef __cplusplus
}
#endif
