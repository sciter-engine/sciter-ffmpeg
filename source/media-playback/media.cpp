/*
 * Copyright (c) 2017 Hugh Bailey <obs.jim@gmail.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>

#include "media.h"
//#include "closest-format.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <libavdevice/avdevice.h>
#include <libavutil/imgutils.h>
#include <libavutil/time.h>

#ifdef __cplusplus
}
#endif

static int64_t base_sys_ts = 0;

static inline int64_t os_gettime_ns() {
	return av_gettime_relative() * 1000;
}

static inline int os_sleep_ms(uint32_t duration) {
	return av_usleep(duration * 1000);
}

static inline struct mp_decode* get_packet_decoder(mp_media_t* media,
	const AVPacket* pkt)
{
	if (media->has_audio && pkt->stream_index == media->a.stream->index)
		return &media->a;
	if (media->has_video && pkt->stream_index == media->v.stream->index)
		return &media->v;

	return NULL;
}

void mp_media_free_packet(struct mp_media *media, AVPacket *pkt)
{
	av_packet_unref(pkt);
	da_push_back(media->packet_pool, &pkt);
}

static int mp_media_next_packet(mp_media_t *media)
{
	AVPacket *pkt;
	AVPacket **const cached = (AVPacket **)da_end(media->packet_pool);
	if (cached) {
		pkt = *cached;
		da_pop_back(media->packet_pool);
	} else {
		pkt = av_packet_alloc();
	}

	int ret = av_read_frame(media->fmt, pkt);
	if (ret < 0) {
		if (ret != AVERROR_EOF && ret != AVERROR_EXIT) {
			char err_str[AV_ERROR_MAX_STRING_SIZE] = { 0 };
			av_make_error_string(err_str, sizeof(err_str), ret);
			av_log(nullptr, AV_LOG_WARNING, "MP: av_read_frame failed: %s (%d)",
				err_str, ret);
		}
		return ret;
	}

	struct mp_decode *d = get_packet_decoder(media, pkt);
	if (d && pkt->size) {
		mp_decode_push_packet(d, pkt);
	} else {
		mp_media_free_packet(media, pkt);
	}

	return ret;
}

static inline bool mp_media_ready_to_start(mp_media_t* m)
{
	if (m->has_audio && !m->a.eof && !m->a.frame_ready)
		return false;
	if (m->has_video && !m->v.eof && !m->v.frame_ready)
		return false;
	return true;
}

static inline bool mp_decode_frame(struct mp_decode* d)
{
	return d->frame_ready || mp_decode_next(d);
}

static inline int get_sws_colorspace(enum AVColorSpace cs)
{
	switch (cs) {
	case AVCOL_SPC_BT709:
		return SWS_CS_ITU709;
	case AVCOL_SPC_FCC:
		return SWS_CS_FCC;
	case AVCOL_SPC_BT470BG:
		return SWS_CS_ITU624;
	case AVCOL_SPC_SMPTE170M:
		return SWS_CS_SMPTE170M;
	case AVCOL_SPC_SMPTE240M:
		return SWS_CS_SMPTE240M;
	case AVCOL_SPC_BT2020_NCL:
		return SWS_CS_BT2020;
	default:
		break;
	}

	return SWS_CS_ITU709;
}

static inline int get_sws_range(enum AVColorRange r)
{
	return r == AVCOL_RANGE_JPEG ? 1 : 0;
}

#define FIXED_1_0 (1 << 16)

static bool mp_media_init_scaling(mp_media_t* m)
{
	int space = get_sws_colorspace(m->v.decoder->colorspace);
	int range = get_sws_range(m->v.decoder->color_range);
	const int* coeff = sws_getCoefficients(space);

	m->swscale = sws_getCachedContext(NULL, m->v.frame->width,
		m->v.frame->height,
		(AVPixelFormat)m->v.frame->format,
		m->v.frame->width,
		m->v.frame->height, m->scale_format,
		SWS_POINT, NULL, NULL, NULL);
	if (!m->swscale) {
		av_log(nullptr, AV_LOG_WARNING, "MP: Failed to initialize scaler");
		return false;
	}

	sws_setColorspaceDetails(m->swscale, coeff, range, coeff, range, 0,
		FIXED_1_0, FIXED_1_0);

	int ret = av_image_alloc(m->scale_pic, m->scale_linesizes,
				 m->v.frame->width, m->v.frame->height,
				 m->scale_format, 32);
	if (ret < 0) {
		av_log(nullptr, AV_LOG_WARNING, "MP: Failed to create scale pic data");
		return false;
	}

	return true;
}

static bool mp_media_prepare_frames(mp_media_t* m)
{
	bool actively_seeking = m->seek_next_ts && m->pause;

	while (!mp_media_ready_to_start(m)) {
		if (!m->eof) {
			int ret = mp_media_next_packet(m);
			if (ret == AVERROR_EOF || ret == AVERROR_EXIT) {
				if (!actively_seeking) {
					m->eof = true;
				} else {
					break;
				}
			} else if (ret < 0) {
				return false;
			}
		}

		if (m->has_video && !mp_decode_frame(&m->v))
			return false;
		if (m->has_audio && !mp_decode_frame(&m->a))
			return false;
	}

	if (m->has_video && m->v.frame_ready && !m->swscale) {
		m->scale_format = AV_PIX_FMT_BGRA; //closest_format((AVPixelFormat)m->v.frame->format);
		if (m->scale_format != m->v.frame->format) {
			if (!mp_media_init_scaling(m)) {
				return false;
			}
		}
	}

	return true;
}

static inline int64_t mp_media_get_next_min_pts(mp_media_t* m)
{
	int64_t min_next_ns = 0x7FFFFFFFFFFFFFFFLL;

	if (m->has_video && m->v.frame_ready) {
		if (m->v.frame_pts < min_next_ns)
			min_next_ns = m->v.frame_pts;
	}
	if (m->has_audio && m->a.frame_ready) {
		if (m->a.frame_pts < min_next_ns)
			min_next_ns = m->a.frame_pts;
	}

	return min_next_ns;
}

static inline int64_t mp_media_get_base_pts(mp_media_t* m)
{
	int64_t base_ts = 0;

	if (m->has_video && m->v.next_pts > base_ts)
		base_ts = m->v.next_pts;
	if (m->has_audio && m->a.next_pts > base_ts)
		base_ts = m->a.next_pts;

	return base_ts;
}

/* maximum timestamp variance in nanoseconds */
#define MAX_TS_VAR 2000000000LL

static inline bool mp_media_can_play_frame(mp_media_t* m, struct mp_decode* d)
{
	return d->frame_ready && (d->frame_pts <= m->next_pts_ns ||
		(d->frame_pts - m->next_pts_ns > MAX_TS_VAR));
}

static void mp_media_next_audio(mp_media_t* m)
{
	struct mp_decode *d = &m->a;
	struct obs_source_audio audio = {0};
	AVFrame *f = d->frame;
	int channels = 0;

	if (!mp_media_can_play_frame(m, d))
		return;

#if LIBAVFORMAT_VERSION_INT < AV_VERSION_INT(59, 19, 100)
	channels = f->channels;
#else
	channels = f->ch_layout.nb_channels;
#endif

	d->frame_ready = false;
	if (!m->a_cb)
		return;

	for (size_t i = 0; i < MAX_AV_PLANES; i++)
		audio.data[i] = f->data[i];

	audio.samples_per_sec = f->sample_rate * m->speed / 100;
	audio.channels = channels;
	audio.format = (AVSampleFormat)f->format;
	audio.frames = f->nb_samples;

	audio.timestamp = m->base_ts + d->frame_pts - m->start_ts +
		m->play_sys_ts - base_sys_ts;

	if (audio.format == AVSampleFormat::AV_SAMPLE_FMT_NONE)
		return;

	m->a_cb(m->opaque, &audio);
}

static void mp_media_next_video(mp_media_t* m, bool preload, bool stopping)
{
	struct mp_decode* d = &m->v;
	struct obs_source_frame* frame = &m->obsframe;
	AVPixelFormat new_format;
	AVColorSpace new_space;
	AVColorRange new_range;
	AVFrame* f = d->frame;

	if (!preload) {
		if (!mp_media_can_play_frame(m, d))
			return;

		d->frame_ready = false;

		if (!m->v_cb)
			return;
	} else if (!d->frame_ready) {
		return;
	}

	bool flip = false;
	if (m->swscale) {
		int ret = sws_scale(m->swscale, (const uint8_t *const *)f->data,
				    f->linesize, 0, f->height, m->scale_pic,
				    m->scale_linesizes);
		if (ret < 0)
			return;

		flip = m->scale_linesizes[0] < 0 && m->scale_linesizes[1] == 0;
		for (size_t i = 0; i < 4; i++) {
			frame->data[i] = m->scale_pic[i];
			frame->linesize[i] = abs(m->scale_linesizes[i]);
		}

	} else {
		flip = f->linesize[0] < 0 && f->linesize[1] == 0;

		for (size_t i = 0; i < MAX_AV_PLANES; i++) {
			frame->data[i] = f->data[i];
			frame->linesize[i] = abs(f->linesize[i]);
		}
	}

	if (flip)
		frame->data[0] -= frame->linesize[0] * (f->height - 1);

	new_format = m->scale_format;
	new_space = f->colorspace;
	new_range = m->force_range == AVColorRange::AVCOL_RANGE_UNSPECIFIED
		? f->color_range
		: m->force_range;

	if (new_format != frame->format || new_space != m->cur_space ||
	    new_range != m->cur_range) {
		bool success;

		frame->format = new_format;
		frame->full_range = new_range == AVColorRange::AVCOL_RANGE_JPEG;

		success = true;/* video_format_get_parameters(new_space, new_range,
							  frame->color_matrix,
							  frame->color_range_min,
							  frame->color_range_max); */

		frame->format = new_format;
		m->cur_space = new_space;
		m->cur_range = new_range;

		if (!success) {
			frame->format = AVPixelFormat::AV_PIX_FMT_NONE;
			return;
		}
	}

	if (frame->format == AVPixelFormat::AV_PIX_FMT_NONE)
		return;

	frame->timestamp = m->base_ts + d->frame_pts - m->start_ts +
			   m->play_sys_ts - base_sys_ts;

	frame->width = f->width;
	frame->height = f->height;
	frame->max_luminance = d->max_luminance;
	frame->flip = flip;
	frame->flags = 0;

	if (!m->is_local_file && !d->got_first_keyframe) {
		if (!f->key_frame)
			return;

		d->got_first_keyframe = true;
	}
	if (preload && !stopping) {
		if (m->seek_next_ts && m->v_seek_cb) {
			m->v_seek_cb(m->opaque, frame);
		} else {
			m->v_preload_cb(m->opaque, frame);
		}
	} else {
		m->v_cb(m->opaque, frame);
	}
}

static void mp_media_calc_next_ns(mp_media_t* m)
{
	int64_t min_next_ns = mp_media_get_next_min_pts(m);
	int64_t delta = min_next_ns - m->next_pts_ns;

	if (m->seek_next_ts) {
		delta = 0;
		m->seek_next_ts = false;
	} else {
#ifdef _DEBUG
		if (m->speed == 100) {
			assert(delta >= 0);
		}
#endif
		if (delta < 0)
			delta = 0;
		if (delta > 3000000000)
			delta = 0;
	}

	m->next_ns += delta;
	m->next_pts_ns = min_next_ns;
}

static void seek_to(mp_media_t* m, int64_t pos)
{
	AVStream* stream = m->fmt->streams[0];
	int64_t seek_pos = pos;
	int seek_flags;

	if (m->fmt->duration == AV_NOPTS_VALUE)
		seek_flags = AVSEEK_FLAG_FRAME;
	else
		seek_flags = AVSEEK_FLAG_BACKWARD;

	AVRational time_base = { 1, AV_TIME_BASE };
	int64_t seek_target = seek_flags == AVSEEK_FLAG_BACKWARD
		? av_rescale_q(seek_pos, time_base,
			stream->time_base)
		: seek_pos;

	if (m->has_seek) {
		int ret = av_seek_frame(m->fmt, 0, seek_target, seek_flags);
		if (ret < 0) {
			char err_str[AV_ERROR_MAX_STRING_SIZE] = { 0 };
			av_make_error_string(err_str, sizeof(err_str), ret);
			av_log(nullptr, AV_LOG_WARNING, "MP: Failed to seek: %s", err_str);
		}
		else {
			m->eof = false;
		}
		if (m->seek_next_ts) {
			m->seek_cb(m->opaque, true);
		}
	}
	if (m->has_video && m->has_seek) {
		mp_decode_flush(&m->v);
		if (m->seek_next_ts && m->pause && m->v_preload_cb &&
			mp_media_prepare_frames(m))
			mp_media_next_video(m, true, false);
	}
	if (m->has_audio && m->has_seek) {
		mp_decode_flush(&m->a);
	}

}

static bool mp_media_reset(mp_media_t* m)
{
	bool stopping;
	bool active;

	int64_t next_ts = mp_media_get_base_pts(m);
	int64_t offset = next_ts - m->next_pts_ns;
	int64_t start_time = m->fmt->start_time;
	if (start_time == AV_NOPTS_VALUE)
		start_time = 0;

	m->eof = false;
	m->base_ts += next_ts;
	m->seek_next_ts = false;

	seek_to(m, start_time);

	m->mutex->lock();
	stopping = m->stopping;
	active = m->active;
	m->stopping = false;
	m->mutex->unlock();

	if (!mp_media_prepare_frames(m))
		return false;

	if (active) {
		if (!m->play_sys_ts)
			m->play_sys_ts = (int64_t)os_gettime_ns();
		m->start_ts = m->next_pts_ns = mp_media_get_next_min_pts(m);
		if (m->next_ns)
			m->next_ns += offset;
	} else {
		m->start_ts = m->next_pts_ns = mp_media_get_next_min_pts(m);
		m->play_sys_ts = (int64_t)os_gettime_ns();
		m->next_ns = 0;
	}

	m->pause = false;

	if (!active && m->v_preload_cb)
		mp_media_next_video(m, true, stopping);
	if (stopping && m->stop_cb)
		m->stop_cb(m->opaque);
	return true;
}

static inline bool mp_media_sleep(mp_media_t* m)
{
	bool timeout = false;

	if (!m->next_ns) {
		m->next_ns = os_gettime_ns();
	} else {
		const uint64_t t = os_gettime_ns();
		if (m->next_ns > t) {
			const uint32_t delta_ms =
				(uint32_t)((m->next_ns - t + 500000) / 1000000);
			if (delta_ms > 0) {
				static const uint32_t timeout_ms = 200;
				timeout = delta_ms > timeout_ms;
				m->sem->wait(timeout ? timeout_ms : delta_ms);
			}
		}
	}

	return timeout;
}

static inline bool mp_media_eof(mp_media_t* m)
{
	bool v_ended = !m->has_video || !m->v.frame_ready;
	bool a_ended = !m->has_audio || !m->a.frame_ready;
	bool eof = v_ended && a_ended;

	if (eof) {
		bool looping;

		m->mutex->lock();
		looping = m->looping;
		if (!looping) {
			m->active = false;
			m->stopping = true;
		}
		m->mutex->unlock();

		mp_media_reset(m);
	}

	return eof;
}

static int interrupt_callback(void* data)
{
	mp_media_t* m = (mp_media_t*)data;
	bool stop = false;
	uint64_t ts = os_gettime_ns();

	if ((ts - m->interrupt_poll_ts) > 20000000) {
		m->mutex->lock();
		stop = m->kill || m->stopping;
		m->mutex->unlock();

		m->interrupt_poll_ts = ts;
	}

	return stop;
}

#define RIST_PROTO "rist"

static bool init_avformat(mp_media_t* m)
{
#if LIBAVFORMAT_VERSION_INT < AV_VERSION_INT(59, 0, 100)
	AVInputFormat* format = NULL;
#else
	const AVInputFormat* format = NULL;
#endif

	if (m->format_name && *m->format_name) {
		format = av_find_input_format(m->format_name);
		if (!format) {
			av_log(nullptr, AV_LOG_INFO,
				"MP: Unable to find input format for "
				"'%s'",
				m->path);
		}
	}

	AVDictionary *opts = NULL;
	bool is_rist = strncmp(m->path, RIST_PROTO, strlen(RIST_PROTO)) == 0;
	if (m->buffering && !m->is_local_file && !is_rist)
		av_dict_set_int(&opts, "buffer_size", m->buffering, 0);

	if (m->ffmpeg_options) {
		aux::utf2w str_options(m->ffmpeg_options);
		sciter::value options = sciter::value::from_string(str_options.c_str(), str_options.length(), CVT_JSON_LITERAL);
		int ret = -22;
		if (options.is_map()) {
			ret = 0;
			options.each_key_value([&](const sciter::value& key, const sciter::value & val) -> bool {
				aux::w2utf str_key(key.to_string()), str_val(val.to_string());
				av_dict_set(&opts, str_key.c_str(), str_val.c_str(), 0);
				return true;
			});
		}
		if (ret) {
			char err_str[AV_ERROR_MAX_STRING_SIZE] = { 0 };
			av_make_error_string(err_str, sizeof(err_str), ret);
			av_log(nullptr, AV_LOG_WARNING,
			     "Failed to parse FFmpeg options: %s\n%s",
				 err_str, m->ffmpeg_options);
		} else {
			av_log(nullptr, AV_LOG_INFO, "Set FFmpeg options: %s",
			     m->ffmpeg_options);
		}
	}

	m->fmt = avformat_alloc_context();
	if (m->buffering == 0) {
		m->fmt->flags |= AVFMT_FLAG_NOBUFFER;
	}
	if (!m->is_local_file) {
		av_dict_set(&opts, "stimeout", "30000000", 0);
		m->fmt->interrupt_callback.callback = interrupt_callback;
		m->fmt->interrupt_callback.opaque = m;
	}

	int ret = avformat_open_input(&m->fmt, m->path, format,
				      opts ? &opts : NULL);
	av_dict_free(&opts);

	if (ret < 0) {
		if (!m->reconnecting)
			av_log(nullptr, AV_LOG_WARNING, "MP: Failed to open media: '%s'",
				m->path);

		if (m->error_cb) {
			m->error_cb(m->opaque, ENOENT);
		}
		return false;
	}

	if (avformat_find_stream_info(m->fmt, NULL) < 0) {
		av_log(nullptr, AV_LOG_WARNING, "MP: Failed to find stream info for '%s'",
			m->path);
		if (m->error_cb) {
			m->error_cb(m->opaque, AVERROR_STREAM_NOT_FOUND);
		}
		return false;
	}

	m->reconnecting = false;
	m->has_video = mp_decode_init(m, AVMEDIA_TYPE_VIDEO, m->hw);
	m->has_audio = mp_decode_init(m, AVMEDIA_TYPE_AUDIO, m->hw);

	if (!m->has_video && !m->has_audio) {
		av_log(nullptr, AV_LOG_WARNING,
			"MP: Could not initialize audio or video: "
			"'%s'",
			m->path);
		if (m->error_cb) {
			m->error_cb(m->opaque, AVERROR_DECODER_NOT_FOUND);
		}
		return false;
	}

	if (m->metadata_cb) {
		m->metadata_cb(m->opaque);
	}

	return true;
}

static void reset_ts(mp_media_t* m)
{
	m->base_ts += mp_media_get_base_pts(m);
	m->play_sys_ts = (int64_t)os_gettime_ns();
	m->start_ts = m->next_pts_ns = mp_media_get_next_min_pts(m);
	m->next_ns = 0;
}

static inline bool mp_media_thread(mp_media_t* m)
{
	//os_set_thread_name("mp_media_thread");

	if (!init_avformat(m)) {
		return false;
	}
	if (!mp_media_reset(m)) {
		return false;
	}
	m->ready_cb(m->opaque);

	int64_t lasttime = av_gettime(), curtime = 0;

	for (;;) {
		bool reset, kill, is_active, seek, pause, reset_time;
		int64_t seek_pos;
		bool timeout = false;

		m->mutex->lock();

		is_active = m->active;
		pause = m->pause;
		reset = m->reset;
		kill = m->kill;
		m->reset = false;
		m->kill = false;

		m->mutex->unlock();

		if (kill) {
			break;
		}
		if (reset) {
			mp_media_reset(m);
			continue;
		}

		if (!is_active || pause) {
			m->sem->wait();
			if (pause)
				reset_ts(m);
		}
		else {
			timeout = mp_media_sleep(m);
		}

		m->mutex->lock();

		reset = m->reset;
		kill = m->kill;
		m->reset = false;
		m->kill = false;

		pause = m->pause;
		seek_pos = m->seek_pos;
		seek = m->seek;
		reset_time = m->reset_ts;
		m->seek = false;
		m->reset_ts = false;

		m->mutex->unlock();

		if (kill) {
			break;
		}
		if (reset) {
			mp_media_reset(m);
			continue;
		}

		if (seek) {
			m->seek_next_ts = true;
			seek_to(m, seek_pos);
			continue;
		}

		if (reset_time) {
			reset_ts(m);
			m->timeupdate_cb(m->opaque);
			continue;
		}

		if (pause)
			continue;

		/* frames are ready */
		if (is_active && !timeout) {
			if (m->has_video)
				mp_media_next_video(m, false, false);
			if (m->has_audio)
				mp_media_next_audio(m);

			if (!mp_media_prepare_frames(m))
				return false;
			if (mp_media_eof(m))
				continue;

			mp_media_calc_next_ns(m);

			curtime = av_gettime();
			if ((curtime - lasttime) > 250000) {
				lasttime = curtime;
				m->timeupdate_cb(m->opaque);
			}
		}
	}

	return true;
}

static void* mp_media_thread_start(void* opaque)
{
	mp_media_t* m = (mp_media_t*)opaque;

	if (!mp_media_thread(m)) {
		if (m->stop_cb) {
			m->stop_cb(m->opaque);
		}
	}

	return NULL;
}

static inline bool mp_media_init_internal(mp_media_t* m,
	const struct mp_media_info* info)
{
	m->mutex = new sciter::sync::mutex();
	m->sem = new sciter::sync::event();
	m->path = nullptr;
	if (nullptr != info->path) {
		int path_len = strlen(info->path);
		m->path = (char*)malloc(path_len + 1);
		strcpy(m->path, info->path);
	}
	m->format_name = nullptr;
	if (nullptr != info->format) {
		int path_len = strlen(info->format);
		m->format_name = (char*)malloc(path_len + 1);
		strcpy(m->format_name, info->format);
	}
	m->ffmpeg_options = nullptr;
	if (nullptr != info->ffmpeg_options) {
		int path_len = strlen(info->ffmpeg_options);
		m->ffmpeg_options = (char*)malloc(path_len + 1);
		strcpy(m->ffmpeg_options, info->ffmpeg_options);
	}
	m->hw = info->hardware_decoding;

	m->thread = new std::thread(mp_media_thread_start, m);
	if (nullptr == m->thread) {
		av_log(nullptr, AV_LOG_WARNING, "MP: Could not create media thread");
		return false;
	}

	m->thread_valid = true;
	return true;
}

bool mp_media_init(mp_media_t* media, const struct mp_media_info* info)
{
	memset(media, 0, sizeof(*media));
	media->opaque = info->opaque;
	media->v_cb = info->v_cb;
	media->a_cb = info->a_cb;
	media->metadata_cb = info->metadata_cb;
	media->stop_cb = info->stop_cb;
	media->ready_cb = info->ready_cb;
	media->timeupdate_cb = info->timeupdate_cb;
	media->seek_cb = info->seek_cb;
	media->error_cb = info->error_cb;
	media->v_seek_cb = info->v_seek_cb;
	media->v_preload_cb = info->v_preload_cb;
	media->force_range = info->force_range;
	media->is_linear_alpha = info->is_linear_alpha;
	media->buffering = info->buffering;
	media->speed = info->speed;
	media->is_local_file = info->is_local_file;
	media->has_seek = true;
	da_init(media->packet_pool);

	if (!info->is_local_file || media->speed < 1 || media->speed > 200)
		media->speed = 100;

	static bool initialized = false;
	if (!initialized) {
		avdevice_register_all();
		avformat_network_init();
		initialized = true;
	}

	if (!base_sys_ts)
		base_sys_ts = (int64_t)os_gettime_ns();

	if (!mp_media_init_internal(media, info)) {
		mp_media_free(media);
		return false;
	}

	return true;
}

static void mp_kill_thread(mp_media_t* m)
{
	if (m->thread_valid) {
		m->mutex->lock();
		m->kill = true;
		m->mutex->unlock();
		m->sem->signal();

		m->thread->join();
	}
}

void mp_media_free(mp_media_t* media)
{
	if (!media)
		return;

	mp_media_stop(media);
	mp_kill_thread(media);
	mp_decode_free(&media->v);
	mp_decode_free(&media->a);
	for (size_t i = 0; i < media->packet_pool.num; i++)
		av_packet_free(&media->packet_pool.array[i]);
	da_free(media->packet_pool);
	avformat_close_input(&media->fmt);
	sws_freeContext(media->swscale);
	av_freep(&media->scale_pic[0]);
	free(media->path);
	if (nullptr != media->format_name) {
		free(media->format_name);
	}
	if (nullptr != media->ffmpeg_options) {
		free(media->ffmpeg_options);
	}
	delete media->mutex;
	delete media->sem;
	memset(media, 0, sizeof(*media));
}

void mp_media_play(mp_media_t* m, bool loop, bool reconnecting)
{
	m->mutex->lock();

	if (m->active || m->eof)
		m->reset = true;

	m->looping = loop;
	m->active = true;
	m->reconnecting = reconnecting;

	m->mutex->unlock();

	m->sem->signal();
}

void mp_media_play_pause(mp_media_t* m, bool pause)
{
	m->mutex->lock();
	if (m->active) {
		m->pause = pause;
		m->reset_ts = !pause;
	}
	m->mutex->unlock();

	m->sem->signal();
}

void mp_media_stop(mp_media_t* m)
{
	m->mutex->lock();
	if (m->active) {
		m->reset = true;
		m->active = false;
		m->stopping = true;
	}
	m->mutex->unlock();

	m->sem->signal();
}

int64_t mp_media_get_current_time(mp_media_t* m)
{
	return mp_media_get_base_pts(m) * (int64_t)m->speed / 100000000LL;
}

int64_t mp_media_get_frames(mp_media_t* m, AVMediaType mediaType)
{
	int64_t frames = 0;

	if (!m->fmt) {
		return 0;
	}

	int stream_index = av_find_best_stream(m->fmt, mediaType,
		-1, -1, NULL, 0);

	if (stream_index < 0) {
		av_log(nullptr, AV_LOG_WARNING, "MP: Getting number of frames failed: No "
			"video stream in media file!");
		return 0;
	}

	AVStream* stream = m->fmt->streams[stream_index];
	if (stream->nb_frames > 0) {
		frames = stream->nb_frames;
	}
	else {
		av_log(nullptr, AV_LOG_DEBUG, "MP: nb_frames not set, estimating using frame "
			"rate and duration");
		AVRational avg_frame_rate = stream->avg_frame_rate;
		frames = (int64_t)ceil((double)m->fmt->duration /
			(double)AV_TIME_BASE *
			(double)avg_frame_rate.num /
			(double)avg_frame_rate.den);
	}

	return frames;
}

int64_t mp_media_get_duration(mp_media_t* m)
{
	return m->fmt ? m->fmt->duration : 0;
}

void mp_media_seek(mp_media_t* m, int64_t pos)
{
	m->mutex->lock();
	if (m->active) {
		m->seek = true;
		m->seek_pos = pos * 1000;
	}
	m->mutex->unlock();

	m->sem->signal();

	m->seek_cb(m->opaque, false);
}

void mp_media_set_speed(mp_media_t* m, int speed)
{
	m->mutex->lock();
	if (m->active) {
		m->speed = speed;
	}
	m->mutex->unlock();

	m->sem->signal();
}

int mp_media_get_speed(mp_media_t* m)
{
	int speed = 0;
	m->mutex->lock();
	if (m->active) {
		speed = m->speed;
	}
	m->mutex->unlock();

	return speed;
}
