#include "sciter-x-video-api.h"
#include "decode.cpp"
#include "media.cpp"

#define MA_NO_ENCODING
#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

namespace ffmpeg {

	// Convert ASCII hex digit to a nibble (four bits, 0 - 15).
	//
	// Use unsigned to avoid signed overflow UB.
	static inline unsigned char hex2nibble(unsigned char c) {
		if (c >= '0' && c <= '9') {
			return c - '0';
		}
		else if (c >= 'a' && c <= 'f') {
			return 10 + (c - 'a');
		}
		else if (c >= 'A' && c <= 'F') {
			return 10 + (c - 'A');
		}
		return 0;
	}

	// Convert ASCII hex string (two characters) to byte.
	//
	// E.g., "0B" => 0x0B, "af" => 0xAF.
	static inline char hex2char(const char* p) {
		return hex2nibble(p[0]) * 16 + hex2nibble(p[1]);
	}

	inline std::string url_encode(const std::string s) {
		std::string encoded;
		for (unsigned int i = 0; i < s.length(); i++) {
			auto c = s[i];
			if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
				encoded = encoded + c;
			}
			else {
				char hex[4];
				snprintf(hex, sizeof(hex), "%%%02x", c);
				encoded = encoded + hex;
			}
		}
		return encoded;
	}

	inline std::string url_decode(const std::string st) {
		std::string decoded;
		const char* s = st.c_str();
		size_t length = strlen(s);
		for (unsigned int i = 0; i < length; i++) {
			if (s[i] == '%') {
				decoded.push_back(hex2char(s + i + 1));
				i = i + 2;
			}
			else if (s[i] == '+') {
				decoded.push_back(' ');
			}
			else {
				decoded.push_back(s[i]);
			}
		}
		return decoded;
	}

	//Reference: https://github.com/mackron/miniaudio/issues/107
	template <class T>
	void interleave_audio_data(std::vector<uint8_t>& interleave_buffer, const obs_source_audio* frame)
	{
		// resize audio interleave buffer
		interleave_buffer.resize(frame->frames * frame->channels * sizeof(T));

		// loop through channels
		for (int32_t i = 0; i < frame->channels; i++)
		{
			// interleave audio data
			T* sp = (T*)frame->data[i];
			T* ep = sp + frame->frames;
			T* dp = (T*)&interleave_buffer[0] + i;
			while (sp < ep)
			{
				*dp = *sp++;
				dp += frame->channels;
			}
		}
	}

	static void FFSampleFmt2MAFmtLayout(AVSampleFormat sampleFmt, ma_format& format, ma_stream_layout& stream_layout) {
		switch (sampleFmt)
		{
		case AV_SAMPLE_FMT_U8:
			format = ma_format_u8;
			stream_layout = ma_stream_layout_interleaved;
			break;

		case AV_SAMPLE_FMT_S16:
			format = ma_format_s16;
			stream_layout = ma_stream_layout_interleaved;
			break;

		case AV_SAMPLE_FMT_S32:
			format = ma_format_s32;
			stream_layout = ma_stream_layout_interleaved;
			break;

		case AV_SAMPLE_FMT_FLT:
			format = ma_format_f32;
			stream_layout = ma_stream_layout_interleaved;
			break;

		case AV_SAMPLE_FMT_U8P:
			format = ma_format_u8;
			stream_layout = ma_stream_layout_deinterleaved;
			break;

		case AV_SAMPLE_FMT_S16P:
			format = ma_format_s16;
			stream_layout = ma_stream_layout_deinterleaved;
			break;

		case AV_SAMPLE_FMT_S32P:
			format = ma_format_s32;
			stream_layout = ma_stream_layout_deinterleaved;
			break;

		case AV_SAMPLE_FMT_FLTP:
			format = ma_format_f32;
			stream_layout = ma_stream_layout_deinterleaved;
			break;

		default:
			format = ma_format_unknown;
			stream_layout = ma_stream_layout_deinterleaved;
			break;
		}
	}


	class mediaplayer
	{
	public:
		using callback_t = std::function<int(const char* evt, const sciter::value&)>;

		enum MediaStatus {
			kMediaUnloaded = -1,
			kMediaStopped = 0,
			kMediaPlaying = 1,
			kMediaPaused = 2,
			kMediaBuffering = 3
		};

		enum MediaError {
			kMediaErrAborted = 1,
			kMediaErrNetwork = 2,
			kMediaErrDecode = 3,
			kMediaErrSrcNotSupported = 4
		};

		static inline sciter::value MediaErrorMake(int errCode) {
			sciter::value media_error;
			media_error.set_item("code", errCode);
			return media_error;
		}

		mediaplayer() {
			status = kMediaUnloaded;
			rendering_site = nullptr;
			isStreaming = false;
		}

		~mediaplayer() {
			cleanup();
		}

		void open(const char* url, const char* options = nullptr) {
			reload(url, options);
		}

		void cleanup() {
			if (nullptr != cur_media.opaque) {
				stop();
				mp_media_free(&cur_media);
				close_audio_device();
			}
			status = kMediaUnloaded;
		}

		void play() {
			if (kMediaStopped == status) {
				mp_media_play(&cur_media, false, false);
				status = kMediaPlaying;
				callback("play", sciter::value::null());
			}
			else if (kMediaPaused == status) {
				mp_media_play_pause(&cur_media, false);
				status = kMediaPlaying;
				callback("play", sciter::value::null());
			}
		}

		void pause() {
			if (kMediaUnloaded == status) {
				return;
			}
			if (kMediaPaused != status) {
				mp_media_play_pause(&cur_media, true);
				status = kMediaPaused;
				callback("pause", sciter::value::null());
			}
		}

		void stop() {
			if (kMediaUnloaded == status) {
				return;
			}
			if (kMediaStopped != status) {
				active_stop = true;
				mp_media_stop(&cur_media);
				stop_evt.wait();
				active_stop = false;
				status = kMediaStopped;
			}
		}

		double duration() {
			if (nullptr == cur_media.fmt) {
				return 0;
			}
			return mp_media_get_duration(&cur_media) / INT64_C(1000);
		}

		double getCurrentTime() {
			if (nullptr == cur_media.fmt) {
				return 0;
			}
			return mp_media_get_current_time(&cur_media);
		}

		void setCurrentTime(double ms) {
			if (nullptr == cur_media.fmt) {
				return;
			}
			int64_t pos = ms;
			mp_media_seek(&cur_media, pos);
		}

		void setPlaybackRate(double rate) {
			if (nullptr == cur_media.fmt) {
				return;
			}
			if (fabs(getPlaybackRate() - rate) < 0.01) {
				return;
			}
			int speed = 100 * fmin(2.0, fmax(0.01, rate));
			mp_media_set_speed(&cur_media, speed);
			callback("ratechange", sciter::value::null());
		}

		double getPlaybackRate() {
			if (nullptr == cur_media.fmt) {
				return 0;
			}
			return mp_media_get_speed(&cur_media) * 0.01;
		}

		void setVolume(double vol) {
			if (fabs(getVolume() - vol) < 0.01) {
				return;
			}
			volume = vol;
			setMuted(vol < 0.01);
		}

		double getVolume() {
			return muted ? 0.0 : volume;
		}

		void setMuted(bool val) {
			muted = val || (volume <= 0.01);
			_setVolume(muted ? 0.0 : volume);
		}

		bool getMuted() {
			return muted;
		}

		int videoWidth() {
			if (nullptr == cur_media.v.stream) {
				return 0;
			}
			return cur_media.v.stream->codecpar->width;
		}

		int videoHeight() {
			if (nullptr == cur_media.v.stream) {
				return 0;
			}
			return cur_media.v.stream->codecpar->height;
		}

		double videoFrameRate() {
			if (nullptr == cur_media.v.stream) {
				return 0.0;
			}
			AVRational rate = cur_media.v.stream->avg_frame_rate;
			return (double)rate.num / rate.den;
		}

		int videoBitRate() {
			if (nullptr == cur_media.v.stream) {
				return 0;
			}
			return cur_media.v.stream->codecpar->bit_rate;
		}

		int audioChannels() {
			if (nullptr == cur_media.a.stream) {
				return 0;
			}
			return cur_media.a.stream->codecpar->channels;
		}

		int audioSampleRate() {
			if (nullptr == cur_media.a.stream) {
				return 0;
			}
			return cur_media.a.stream->codecpar->sample_rate;
		}

		int audioBitRate() {
			if (nullptr == cur_media.a.stream) {
				return 0;
			}
			return cur_media.a.stream->codecpar->bit_rate;
		}

		sciter::om::hasset<sciter::video_destination> rendering_site;
		callback_t callback;
		bool isStreaming;


		void reload(const char* url, const char* options = nullptr) {
			cleanup();

			callback("loadstart", sciter::value::null());
			if (0 == strlen(url)) {
				error_occured(this, kMediaErrSrcNotSupported);
				return;
			}
			std::string lower_url = url, file_url;
			std::transform(lower_url.begin(), lower_url.end(), lower_url.begin(), tolower);

			mp_media_info info;
			info.opaque = this;
			if (nullptr == strstr(url, "://")) {
				info.is_local_file = true;
				file_url = url;
			}
			else if (0 == strncmp(lower_url.c_str(), "file://", 7)) {
				info.is_local_file = true;
				file_url = url_decode(url + 7);
			}
			else {
				info.is_local_file = false;
				file_url = url;
			}
			info.path = file_url.c_str();
			info.v_cb = get_frame;
			info.v_preload_cb = preload_frame;
			info.v_seek_cb = seek_frame;
			info.a_cb = get_audio;
			info.metadata_cb = get_metadata;
			info.stop_cb = media_stopped;
			info.ready_cb = media_ready;
			info.seek_cb = media_seek;
			info.timeupdate_cb = time_update;
			info.error_cb = error_occured;
			info.format = nullptr;
			info.buffering = 2 * 1024 * 1024;
			info.speed = 100;
			info.force_range = AVCOL_RANGE_UNSPECIFIED;
			info.is_linear_alpha = false;
			info.hardware_decoding = true;
			info.reconnecting = true;
			info.ffmpeg_options = options;
			status = mp_media_init(&cur_media, &info) ? kMediaStopped : kMediaUnloaded;
		}

		static void time_update(void* opaque) {
			mediaplayer* player = (mediaplayer*)opaque;
			player->callback("timeupdate", sciter::value::null());
		}

		static void media_seek(void* opaque, bool seeked) {
			mediaplayer* player = (mediaplayer*)opaque;
			if (seeked) {
				player->callback("seeked", sciter::value::null());
			}
			else {
				player->callback("seeking", sciter::value::null());
			}
		}

		static void error_occured(void* opaque, int code) {
			MediaError errCode;
			switch (code)
			{
			case ENOENT:
				errCode = kMediaErrSrcNotSupported;
				break;
			case AVERROR_DECODER_NOT_FOUND:
				errCode = kMediaErrDecode;
				break;
			default:
				errCode = kMediaErrAborted;
				break;
			}

			mediaplayer* player = (mediaplayer*)opaque;
			sciter::value err_media = mediaplayer::MediaErrorMake(errCode);
			player->callback("mediaerror", err_media);
		}

		static void media_stopped(void* opaque)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			player->stop_proc();
		}

		void stop_proc() {
			if (kMediaStopped == status) {
				return;
			}
			close_audio_device();
			if (nullptr != rendering_site) {
				rendering_site->stop_streaming();
			}
			status = kMediaStopped;
			isStreaming = false;
			if (active_stop) {
				stop_evt.signal();
			}

			callback("ended", sciter::value::null());
		}

		static void get_audio(void* opaque, struct obs_source_audio* a)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			player->render_audio(a);
		}

		bool open_audio_device(ma_format format, int channels, int samples_per_sec) {
			ma_device_config device_config = ma_device_config_init(ma_device_type::ma_device_type_playback);
			device_config.playback.format = format;
			device_config.playback.channels = channels;
			device_config.sampleRate = samples_per_sec;
			device_config.performanceProfile = ma_performance_profile_low_latency;
			device_config.dataCallback = read_audio_cb;
			device_config.pUserData = this;
			ma_result ret = ma_device_init(nullptr, &device_config, &audio_device);
			if (ret != MA_SUCCESS) {
				return false;
			}
			audio_device.masterVolumeFactor = muted ? 0.0 : volume;
			ret = ma_device_start(&audio_device);
			if (ret != MA_SUCCESS) {
				return false;
			}
			return true;
		}

		void close_audio_device() {
			ma_device_uninit(&audio_device);
			clear_audio_cache();
		}

		void _setVolume(double vol) {
			if (nullptr != audio_device.pContext) {
				audio_device.masterVolumeFactor = vol;
			}
			callback("volumechange", sciter::value::null());
		}

		void render_audio(struct obs_source_audio* a) {
			if (kMediaStopped == status) {
				return;
			}

			ma_result ret = MA_ERROR;
			ma_format format = ma_format_unknown;
			ma_stream_layout stream_layout = ma_stream_layout_interleaved;
			FFSampleFmt2MAFmtLayout(a->format, format, stream_layout);

			if (nullptr == audio_device.pUserData) {
				open_audio_device(format, a->channels, a->samples_per_sec);
			}
			else if((a->channels != audio_device.playback.channels)
				|| (format != audio_device.playback.format)
				|| (a->samples_per_sec != audio_device.sampleRate)) {
				close_audio_device();
				open_audio_device(format, a->channels, a->samples_per_sec);
			}

			// audio data deinterleaved?
			uint8_t bytes_per_sample = ma_get_bytes_per_sample(audio_device.playback.format);
			if (stream_layout == ma_stream_layout_deinterleaved)
			{
				std::vector<uint8_t> interleave_buffer;
				// interleave audio data
				if (1 == bytes_per_sample)
				{
					interleave_audio_data<uint8_t>(interleave_buffer, a);
				}
				else if (2 == bytes_per_sample)
				{
					interleave_audio_data<uint16_t>(interleave_buffer, a);
				}
				else/* if (ma_get_bytes_per_sample(m_sys->m_audio_device_config.playback.format) == 4)*/
				{
					interleave_audio_data<uint32_t>(interleave_buffer, a);
				}
				audio_mutex.lock();
				audio_buffer_list.insert(audio_buffer_list.end(), interleave_buffer.begin(), interleave_buffer.end());
				audio_mutex.unlock();
			}
			else {
				int numOfBytes = a->frames * a->channels * bytes_per_sample;

				audio_mutex.lock();
				int oldSize = audio_buffer_list.size();
				audio_buffer_list.resize(oldSize + numOfBytes);
				memcpy(&audio_buffer_list[0] + oldSize, a->data[0], numOfBytes);
				audio_mutex.unlock();
			}
		}

		static void get_metadata(void* opaque)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			player->callback("loadedmetadata", sciter::value::null());
		}

		static void media_ready(void* opaque)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			player->callback("loadeddata", sciter::value::null());
		}

		static void preload_frame(void* opaque, struct obs_source_frame* f)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			get_frame(opaque, f);
		}

		static void seek_frame(void* opaque, struct obs_source_frame* f)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			get_frame(opaque, f);
		}

		static void get_frame(void* opaque, struct obs_source_frame* f)
		{
			mediaplayer* player = (mediaplayer*)opaque;
			player->render_video(f);
		}

		void render_video(struct obs_source_frame* f) {
			if (AV_PIX_FMT_BGRA != f->format) {
				return;
			}
			if (nullptr == rendering_site) {
				return;
			}
			if (!isStreaming) {
				isStreaming = true;
				rendering_site->start_streaming(f->width, f->height, sciter::COLOR_SPACE_RGB32);
			}
			//rendering_site->render_frame_with_stride(f->data[0], f->linesize[0] * f->height * 4, f->linesize[0]);
			rendering_site->render_external_frame(f->data[0], f->linesize[0] * f->height, f->linesize[0], nullptr, f);
		}

		void clear_audio_cache() {
			sciter::sync::critical_section cs(audio_mutex);
			audio_buffer_list.clear();
		}

		static void read_audio_cb(ma_device* audio_device, void* output_buffer, const void* input_buffer, ma_uint32 frame_count)
		{
			mediaplayer* player = (mediaplayer*)audio_device->pUserData;
			player->read_audio_proc(audio_device, output_buffer, input_buffer, frame_count);
		}

		void read_audio_proc(ma_device* audio_device, void* output_buffer, const void* /*input_buffer*/, ma_uint32 frame_count) {
			// set audio frame size
			uint32_t audio_frame_size = audio_device->playback.channels * ma_get_bytes_per_sample(audio_device->playback.format);
			uint32_t frame_bytes = audio_frame_size * frame_count;

			// stopping output?
			if (status == kMediaStopped) {
				return;
			}

			// lock audio buffer mutex
			sciter::sync::critical_section cs(audio_mutex);

			// read more bytes
			uint32_t buffer_size = audio_buffer_list.size();
			if (buffer_size >= frame_bytes) {
				memcpy(output_buffer, audio_buffer_list.data(), frame_bytes);
				audio_buffer_list.erase(audio_buffer_list.begin(), audio_buffer_list.begin() + frame_bytes);
			}
		}

		mp_media cur_media = {};
		MediaStatus status;

		sciter::sync::mutex audio_mutex;
		bool active_stop;
		sciter::sync::event stop_evt;
		std::vector<uint8_t> audio_buffer_list;

		ma_device  audio_device = {};
		bool muted = false;
		double volume = 1.0;

	};

}
