#include "sciter-x.h"
#include "sciter-x-behavior.h"
#include "media-playback/sciter_mediaplayer.hpp"

namespace sciter
{
	/*
	BEHAVIOR: ffmpeg
	   - provides mutlimedia playback
	COMMENTS:
	   <video style="behavior:ffmpeg video" />
	   <audio style="behavior:ffmpeg" />
	SAMPLE:
	   See: samples/ffmpeg/ffmpeg.htm
	*/

	struct sciter_ffmpeg : public event_handler
	{
		HELEMENT this_element;
		bool autoplay;

		ffmpeg::mediaplayer* this_player;

		// ctor
		sciter_ffmpeg() : autoplay(false), this_element(0), this_player(nullptr) {}
		virtual ~sciter_ffmpeg() {}

		virtual bool subscription(HELEMENT he, UINT& event_groups) override
		{
			event_groups = HANDLE_BEHAVIOR_EVENT | HANDLE_ATTRIBUTE_CHANGE;
			return true;
		}

		virtual void attached(HELEMENT he) override
		{
			this_element = he;
			this_player = new ffmpeg::mediaplayer();
			this_player->callback = [&](const char* evt, const sciter::value& param) -> int{
				sciter::string evtName = aux::utf2w(evt).c_str();
				SBOOL handled = false;
				BEHAVIOR_EVENT_PARAMS evtParam = { 0 };
				evtParam.cmd = CUSTOM;
				evtParam.heTarget = evtParam.he = this_element;
				evtParam.name = evtName.c_str();
				evtParam.data = param;
				return SciterFireEvent(&evtParam, true, &handled);
			};

			on_src_changed();
		}

		virtual void detached(HELEMENT he) override
		{
			delete this_player;
			asset_release();
		}

		// notification events from builtin behaviors - synthesized events: BUTTON_CLICK, VALUE_CHANGED
		// see enum BEHAVIOR_EVENTS
		virtual bool handle_event(HELEMENT he, BEHAVIOR_EVENT_PARAMS& params)
		{
			bool handled = on_event(he, params.heTarget, (BEHAVIOR_EVENTS)params.cmd, params.reason);
			if (CUSTOM == params.cmd) {
				sciter::string name = params.name;
				if (0 == name.compare(WSTR("loadeddata"))) {
					if (autoplay) {
						play();
					}
				}
			}
			return handled;
		}

		virtual bool on_event(HELEMENT he, HELEMENT target, BEHAVIOR_EVENTS type, UINT_PTR reason) override
		{
			if (type != VIDEO_BIND_RQ)
				return false;
			// we handle only VIDEO_BIND_RQ requests here

			if (!reason)
				return true; // first phase, consume the event to mark as we will provide frames 

			this_player->rendering_site = (sciter::video_destination*)reason;
			return true;
		}

		virtual void on_attribute_change(HELEMENT he, LPCSTR name, LPCWSTR value) override
		{
			sciter::string str_val = (nullptr != value) ? value : WSTR("");
			if (0 == strcmp(name, "muted")) {
				if (nullptr == value) {
					setMuted(false);
				}
				else {
					setMuted(0 != str_val.compare(WSTR("false")));
				}
			}
			else if (0 == strcmp(name, "autoplay")) {
				if (nullptr == value) {
					setAutoplay(false);
				}
				else {
					setAutoplay(0 != str_val.compare(WSTR("false")));
				}
			}
			else if (0 == strcmp(name, "options")) {
				if (nullptr == value) {
					setOptions(sciter::value::nothing());
				}
				else {
					setOptions(sciter::value::make_string(value));
				}
			}
			else if (0 == strcmp(name, "src")) {
				on_src_changed();
			}
		}

		void on_src_changed() {
			this_player->cleanup();

			dom::element self = dom::element(this_element);
			unsigned int count = self.get_attribute_count();
			for (unsigned int idx = 0; idx != count; idx++) {
				sciter::astring attr_name = self.get_attribute_name(idx);
				if (0 == attr_name.compare("muted")) {
					setMuted(true);
				}
				else if (0 == attr_name.compare("autoplay")) {
					setAutoplay(true);
				}
			}

			if (autoplay) {
				load();
			}
		}

		void load() {
			sciter::string src = getSrc(), options = getOptions();
			aux::w2utf str_src(src.c_str()), str_options(options.c_str());
			this_player->open(str_src.c_str(), str_options.length() > 0 ? str_options.c_str() : nullptr);
		}

		void play() {
			this_player->play();
		}

		void pause() {
			this_player->pause();
		}

		void stop() {
			this_player->stop();
		}

		bool setSrc(const sciter::value& src) {
            sciter::string strSrc = src.to_string();
			dom::element self = dom::element(this_element);
			self.set_attribute("src", strSrc.c_str());
			return true;
		}

		sciter::string getSrc() {
			dom::element self = dom::element(this_element);
			return self.get_attribute("src");
		}

		bool setOptions(const sciter::value& options) {
			dom::element self = dom::element(this_element);
			if (options.is_string()) {
				sciter::string strOptions = options.to_string();
				self.set_attribute("options", strOptions.c_str());
			}
			else if (options.is_object()) {
				sciter::string strOptions = options.to_string(CVT_JSON_LITERAL);
				self.set_attribute("options", strOptions.c_str());
			}
			else {
				self.remove_attribute("options");
			}
			return true;
		}

		sciter::string getOptions() {
			dom::element self = dom::element(this_element);
			return self.get_attribute("options");
		}

		double duration() {
			return this_player->duration();
		}

		double getCurrentTime() {
			return this_player->getCurrentTime();
		}

		bool setCurrentTime(double ms) {
			this_player->setCurrentTime(ms);
			return true;
		}

		bool setPlaybackRate(double rate) {
			this_player->setPlaybackRate(rate);
			return true;
		}

		double getPlaybackRate() {
			return this_player->getPlaybackRate();
		}

		bool setVolume(double vol) {
			this_player->setVolume(vol);
			return true;
		}

		double getVolume() {
			return this_player->getVolume();
		}

		bool setMuted(bool val) {
			this_player->setMuted(val);
			dom::element self = dom::element(this_element);
			if (val) {
				self.set_attribute("muted", WSTR("muted"));
			}
			else {
				self.remove_attribute("muted");
			}
			return true;
		}

		double getMuted() {
			return this_player->getMuted();
		}

		bool setAutoplay(bool val) {
			autoplay = val;
			dom::element self = dom::element(this_element);
			if (val) {
				self.set_attribute("autoplay", WSTR("autoplay"));
			}
			else {
				self.remove_attribute("autoplay");
			}
			return true;
		}

		bool getAutoplay() {
			return autoplay;
		}

		int videoWidth() {
			return this_player->videoWidth();
		}

		int videoHeight() {
			return this_player->videoHeight();
		}

		double videoFrameRate() {
			return this_player->videoFrameRate();
		}

		int videoBitRate() {
			return this_player->videoBitRate();
		}

		int audioChannels() {
			return this_player->audioChannels();
		}

		int audioSampleRate() {
			return this_player->audioSampleRate();
		}

		int audioBitRate() {
			return this_player->audioBitRate();
		}

		SOM_PASSPORT_BEGIN_EX(ffmpeg, sciter_ffmpeg)
			SOM_PROPS(
				SOM_VIRTUAL_PROP(src, getSrc, setSrc),
				SOM_VIRTUAL_PROP(options, getOptions, setOptions),
				SOM_RO_VIRTUAL_PROP(duration, duration),
				SOM_VIRTUAL_PROP(currentTime, getCurrentTime, setCurrentTime),
				SOM_VIRTUAL_PROP(playbackRate, getPlaybackRate, setPlaybackRate),
				SOM_VIRTUAL_PROP(volume, getVolume, setVolume),
				SOM_VIRTUAL_PROP(muted, getMuted, setMuted),
				SOM_VIRTUAL_PROP(autoplay, getAutoplay, setAutoplay),
				SOM_RO_VIRTUAL_PROP(videoWidth, videoWidth),
				SOM_RO_VIRTUAL_PROP(videoHeight, videoHeight),
				SOM_RO_VIRTUAL_PROP(videoFrameRate, videoFrameRate),
				SOM_RO_VIRTUAL_PROP(videoBitRate, videoBitRate),
				SOM_RO_VIRTUAL_PROP(audioChannels, audioChannels),
				SOM_RO_VIRTUAL_PROP(audioSampleRate, audioSampleRate),
				SOM_RO_VIRTUAL_PROP(audioBitRate, audioBitRate)
			)
			SOM_FUNCS(
				SOM_FUNC(load),
				SOM_FUNC(play),
				SOM_FUNC(pause),
				SOM_FUNC(stop)
			)
			SOM_PASSPORT_END
	};

}

#ifdef SCITERFFMPEG_EXPORTS

/** The SciterBehaviorFactory of this module */
SBOOL SCAPI ThisBehaviorFactory(LPCSTR name, HELEMENT he, LPElementEventProc* elProc, LPVOID* elTag)
{
	if (aux::chars_of(name) != const_chars("ffmpeg"))
	{
		return FALSE;
	}
	sciter::sciter_ffmpeg* ffmpeg = new sciter::sciter_ffmpeg();
	ffmpeg->asset_add_ref();
	*elProc = ffmpeg->element_proc;
	*elTag = ffmpeg;
	return TRUE;
}

extern "C"
{
	/** SciterBehaviorFactoryInit is the only exported function that Sciter runtime will call to initialize Sciter component extension library.
	 * \param psapi[in] \b ISciterAPI*, instance of Sciter API interface provided by Sciter runtime to the library.
	 * \param pfactory[out] \b SciterBehaviorFactory**, instance of behavior factory that will create behaviors of this library.
	 * \return TRUE on success.
	 **/
#ifndef WINDOWS
	__attribute__((visibility("default")))
#endif
		SBOOL SCAPI SciterBehaviorFactoryInit(ISciterAPI* psapi, SciterBehaviorFactory** pfactory)
	{
		_SAPI(psapi); // set reference to Sciter API provided by host application including scapp(quark)
		*pfactory = ThisBehaviorFactory;
		return TRUE;
	}
}

#else

namespace sciter
{
	struct sciter_ffmpeg_factory : public behavior_factory
	{
		sciter_ffmpeg_factory() : behavior_factory("ffmpeg") {}

		// the only behavior_factory method:
		virtual event_handler* create(HELEMENT he)
		{
			return new sciter_ffmpeg();
		}
	};

	// instantiating and attaching it to the global list
	sciter_ffmpeg_factory sciter_ffmpeg_factory_instance;
}

#endif
